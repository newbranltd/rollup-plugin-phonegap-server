import opn from 'opn';
import { spawn } from 'child_process';
import 'util';
import merge from 'lodash.merge';

/**
 * Wrapper for phonegap server cmd + opn
 */
// import { join } from 'path';
/**
 * there are several built in options for the phonegap serve command which we need to replicate here
 * 
 * --port, -p <n>       port for web server (default: 3000)
 * --autoreload         enable app refresh on file changes (default: true)
 * --no-autoreload      disable app refresh on file changes
 * --browser            enable desktop browser support (default: true)
 * --no-browser         disable desktop browser support
 * --localtunnel        enable a local tunnel for public access (default: false)
 */
/**
 * main
 * @param {object} options to config
 * @return {object} execute by rollup
 */
function phonegapServer(options) {
  if ( options === void 0 ) options = {};

  // const stockPort = 3000; // @TODO
  // Force these options on config
  var config = merge({
    opn: {
      url: 'http://localhost', // <-- need another step to config this
      option: {}
    },
    spawn: {
      env: process.env,
      cwd: process.cwd()
    },
    serve: {
      port: 3000,
      autoreload: true,
      browser: true,
      localtunnel: null
    },
    verbose: true
  }, options);
  // display the config @BUG this cause some unexpected behavior 
  /*
  if (config.verbose === true) {
    console.info(inspect(config, { showHidden: true, depth: null }));
  }
  */
  // start 
  var es;
  var open = false;
  // Open the browser
  var openBrowser = function () {
    if (!open) {
      open = true;
      var url = [config.opn.url, config.serve.port].join(':');
      opn(url, config.opn.option);
    }
  };
  // run the serve cmd
  var start = function () {
    var flags = ['serve'];
    if (config.serve.autoreload === false) {
      flags.push('--no-autoreload');
    }
    if (config.serve.browser === false) {
      flags.push('--no-browser');
    }
    if (config.serve.localtunnel === true) {
      flags.push('--localtunnel');
    }
    es = spawn('phonegap', flags, config.spawn);
    es.stdout.on('data', function (data) {
      if (config.verbose) {
        console.info(data.toString('utf8'));
      }
      openBrowser();
    });
    es.stderr.on('data', function (data) {
      if (config.verbose) {
        console.warn(data.toString('utf8'));
      }
      openBrowser();
    });
  };

  // listen to signal and close it
  ['SIGINT', 'SIGTERM'].forEach(function (signal) {
    process.on(signal, function () {
      process.exit();
    });
  });
  var running;
  // return
  return {
    name: 'phonegapServer',
    ongenerate: function ongenerate () {
      if (!running) {
        running = true;
        start();
      }
    }
  }
}

export default phonegapServer;
