# rollup-plugin-phonegap-server

This is for a phonegap project that use rollup during the development. Basically it will execute the `phonegap serve` call inside the `www` folder, also using opn to open browser. 

## Usage 

```js
import buble from 'rollup-plugin-buble'
import phonegapServer from 'rollup-plugin-phonegap-server'
// see next section
const config = {} 
// run
export default {
  input: 'src/index.js',
  output: 'dist/index.js',
  plugins: [
    buble(),
    phonegapServer(config)
  ],
  onwarn ({ code, message }) {
    if (code !== 'UNRESOLVED_IMPORT') {
      console.warn(message)
    }
  }
}

export default config
```

When you run your rollup ops, the last op will be calling the `phonegap serve` serve command into the www folder 
(you must execute the rollup ops on the root level, unless you pass the `spawn.cwd` option to tell the plugin where 
the www folder is. Then it will open the site with your default browser. You can change this behavior by passing the 
`opn.option` option to the config, see [opn](https://www.npmjs.com/package/opn) website for more information.  

## Config

You can pass a config object to the `rollup-plugin-phonegap-server`

```js
const config = {
  opn: {
    url: 'http://localhost', // this will construct with the serve.port together
    option: {} // see https://www.npmjs.com/package/opn
  },
  spawn: {
    env: process.env,
    cwd: join(process.cwd(), 'www')
  },
  serve: {
    port: 3000,
    browser: true,
    autoreload: true,
    localtunnel: false
  },
  verbose: true
};
```

By default the `verbose` option will output all the phonegap generate message, which is very useful during development.
You could choice to turn it off by passing `verbose: false`.

**config option for phonegap serve**

You can pass extra option to the `phonegap serve` command according to [phonegap cli documentation](http://docs.phonegap.com/references/phonegap-cli/serve/)

- port (default 3000) 
- browser (default true) if set to `false` then it will pass the `--no-browser` flag
- autoreload (default true) if set to `false` then it will pass the `--no-autoreload` flag 
- localtunnel (default false) if set to `true` then it will pass the `--localtunnel` flag

**config option for opn** 

Please note the url option. Please do not pass any port number. This will get constructed during the run time as follow 

```js
const url = [config.opn.url, config.serve.port].join(':'); 
```

For the extra option for opn, please consult their [documentation](https://www.npmjs.com/package/opn)

--- 

MIT (c) NEWBRAN LTD 2018